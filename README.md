# BB_Automation_Drill

Task  | Class | Description
------------- | ------------- | -------------
3 | AdminLoginTest | Verifies ability to log in as admin 
7 | AdminMenuTest | Verifies that all tabs accessible in admin menu
8 | MainPageProductsStickersTest | Verifies presence of stickers for each product displayed on the main shop page
9.1 | AdminMenuCountriesSortingTest | Verifies sorting of countries and states at the admin's Countries tab
9.2 | AdminMenuGeozonesSortingTest | Verifies sorting of geozones and geozone sub-items at the admin's Geozones tab
10 | MainPageProductToProductPageComparsionTest | Verifies products is represented correctly both at main shop page and specific product page
11 | UserRegistrationTest | Verifies ability to register and log in as new shop user (customer)
12 | AdminMenuCatalogAddNewProductTest | Verifies ability to add new default product from admin menu (Catalog page)
13 | CartAddingRemovingProductsTest | Verifies ability to add several products to the cart and then remove them
14 | AdminMenuAddNewCountyTest | Verifies that links are opened in the new tabs at Admin menu / Countries / Add New Country page
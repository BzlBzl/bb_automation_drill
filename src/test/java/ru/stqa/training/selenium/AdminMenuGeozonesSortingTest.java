package ru.stqa.training.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.stqa.training.entities.DriverProvider;
import ru.stqa.training.pages.AdminLoginPage;
import ru.stqa.training.pages.AdminMenuPage;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/** Task 9.2
 * Verifies sorting of geozones and geozone sub-items at the admin's Geozones tab
 */
public class AdminMenuGeozonesSortingTest {

    private String loginName = "admin";
    private String loginPassword = "admin";
    private DriverProvider.Browser driverType = DriverProvider.Browser.CHROME;

    private WebDriver driver;
    private WebDriverWait wait;
    static Logger log = LogManager.getLogger(AdminMenuGeozonesSortingTest.class);

    @Before
    public void start() {
        driver = DriverProvider.createDriver(driverType);
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void test() {
        try {
            (new AdminLoginPage(driver)).doNavigateAndLogin(wait, loginName, loginPassword);
        } catch (Exception e) {
            log.error("[FAILED] Admin logging failed");
            throw e;
        }
        log.info("Admin logged in successfully");

        AdminMenuPage menuPage = new AdminMenuPage(driver);
        try {
            menuPage.switchToGeozones();
        } catch (Exception e) {
            log.error("[FAILED] Failed to switch to Geozones menu tab");
            throw e;
        }
        log.info("Switched to Geozones tab");

        List<String> geozonesURLs;
        try {
            geozonesURLs = menuPage.getGeozonesURLsList();
            assertFalse("[FAILED] There are no geozones found!", geozonesURLs.isEmpty());
        } catch (Exception e) {
            log.error("[FAILED] No Geozones detected in the list!");
            throw e;
        }
        log.info("Found {} geozones to check", geozonesURLs.size());

        for (String url : geozonesURLs) {
            try {
                driver.navigate().to(url);
            } catch (Exception e) {
                log.error("[FAILED] Failed to navigate to geozone edit page {}", url);
                throw e;
            }
            log.info("Navigated to {}", url);

            List<String> selectedSubitemNames;
            try {
                selectedSubitemNames = menuPage.getGeozoneSubitemSelectedNames();
            } catch (Exception e) {
                log.error("[FAILED] Failed to find geozones subitems!");
                throw e;
            }

            if (selectedSubitemNames.isEmpty()) {
                log.info("No geozone subitems!");
            } else {
                log.info("Found {} geozone subitems", selectedSubitemNames.size());
            }

            String lastName = "";
            for (String name : selectedSubitemNames) {
                assertTrue(
                    String.format("[FAILED] Wrong items ordering: %s vs %s", name, lastName)
                    , name.compareTo(lastName) >= 0
                );
                log.debug("Sorting verified for {}", name);
            }

            log.info("[OK] Sorting verified for {}", url);
        }

        log.info("[Success!] All subitems for all geozones are verified!");

    }

    @After
    public void stop() {
        driver.quit();
        driver = null;
    }
}

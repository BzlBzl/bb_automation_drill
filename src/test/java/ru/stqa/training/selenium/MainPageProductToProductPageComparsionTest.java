package ru.stqa.training.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.stqa.training.entities.DriverProvider;
import ru.stqa.training.entities.Product;
import ru.stqa.training.pages.ShopMainPage;
import ru.stqa.training.pages.ShopProductPage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/** Task 10
 * Verifies products is represented correctly both at main shop page and specific product page.
 */

public class MainPageProductToProductPageComparsionTest {

    private DriverProvider.Browser driverType = DriverProvider.Browser.CHROME;

    private WebDriver driver;
    private WebDriverWait wait;
    static Logger log = LogManager.getLogger(AdminMenuGeozonesSortingTest.class);

    @Before
    public void start() {
        driver = DriverProvider.createDriver(driverType);
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void test() {
        ShopMainPage mainPage = new ShopMainPage(driver);
        try {
            mainPage.navigateTo();
        } catch (Exception e) {
            log.error("[FAILED] Failed to navigate to shop's main page");
            throw e;
        }
        log.info("Navigated to Main page");

        Product mainPageProduct;
        Product product;
        try {
            WebElement campaignItem = mainPage.getCampaignProductsItems().get(0);
            mainPageProduct = mainPage.getProduct(campaignItem);

            campaignItem.click();
        } catch (Exception e) {
            log.error("[FAILED] Failed to get product from main page");
            throw e;
        }
        log.info("Navigating to Product Page");

        try {
            ShopProductPage productPage = new ShopProductPage(driver);
            product = productPage.getProduct();
        } catch (Exception e) {
            log.error("[FAILED] Failed to get product from product page");
            throw e;
        }

        // --- Verification
        assertEquals(
            String.format("[FAILED] Products are not match! %s vs %s", mainPageProduct.toString(), product.toString())
            , mainPageProduct
            , product
        );
        log.info("[OK] Products data compared successfully");

        assertTrue("[FAILED] [Main page] Promo price is bigger than normal", mainPageProduct.verifyPromo2NormalPriceRatio());
        assertTrue("[FAILED] [Product page] Promo price is bigger than normal", product.verifyPromo2NormalPriceRatio());
        log.info("[OK] Products Normal/Promo prices ratios are verified");

        assertTrue("[FAILED] [Main page] Product normal price decoration is wrong", mainPageProduct.verifyNormalPriceDecoration());
        assertTrue("[FAILED] [Product page] Product normal price decoration is wrong", product.verifyNormalPriceDecoration());
        log.info("[OK] Products normal price decorations are verified");

        assertTrue("[FAILED] [Main page] Product promo price decoration is wrong", mainPageProduct.verifyPromoPriceDecoration());
        assertTrue("[FAILED] [Product page] Product promo price decoration is wrong", product.verifyPromoPriceDecoration());
        log.info("[OK] Products promo price decorations are verified");

        log.info("[Success] Products compared successfully!");
    }

    @After
    public void stop() {
        driver.quit();
        driver = null;
    }
}

package ru.stqa.training.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.stqa.training.entities.DriverProvider;
import ru.stqa.training.pages.AdminLoginPage;
import ru.stqa.training.pages.AdminMenuAddNewCountryPage;
import ru.stqa.training.pages.AdminMenuPage;

import java.rmi.server.ExportException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/** Task 14
 * Verifies that links are opened in the new tabs at Admin menu / Countries / Add New Country page
 */
public class AdminMenuAddNewCountyTest {

    private String loginName = "admin";
    private String loginPassword = "admin";
    private DriverProvider.Browser driverType = DriverProvider.Browser.CHROME;

    private WebDriver driver;
    private WebDriverWait wait;
    static Logger log = LogManager.getLogger(AdminLoginTest.class);

    @Before
    public void start() {
        driver = DriverProvider.createDriver(driverType);
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void test() {
        try {
            (new AdminLoginPage(driver)).doNavigateAndLogin(wait, loginName, loginPassword);
        } catch (Exception e) {
            log.error("[FAIL] Failed to log in as admin");
            throw e;
        }
        log.info("Logged in as admin");

        try {
            AdminMenuPage menuPage = new AdminMenuPage(driver);
            menuPage.switchToCountries();
            menuPage.clickAddNewCountry();
            menuPage = null;
        } catch (Exception e) {
            log.error("[FAIL] Failed to navigate to Add New Country page");
            throw e;
        }
        log.info("Navigated to Add New Country page");

        String windowHandle = driver.getWindowHandle();
        List<WebElement> outerLinks = (new AdminMenuAddNewCountryPage(driver)).getOuterLinks();

        for (WebElement link : outerLinks) {
            log.info("Clicking outer link {}", link.getAttribute("href"));
            link.click();

            wait.until(ExpectedConditions.numberOfWindowsToBe(2));

            ArrayList tabs = new ArrayList(driver.getWindowHandles());
            log.debug("[D] Total number of tabs {}. Switching to new tab", tabs.size());
            driver.switchTo().window(tabs.get(1).toString());
            driver.close();
            log.debug("[D] Closing new tab");

            driver.switchTo().window(windowHandle);
            log.debug("[D] Switching to main tab");
        }

        log.info("[Success!] All outer links are opened in new tabs!");
    }

    @After
    public void stop() {
        driver.quit();
        driver = null;
    }
}

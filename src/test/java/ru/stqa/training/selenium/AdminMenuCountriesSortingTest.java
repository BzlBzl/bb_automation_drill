package ru.stqa.training.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.stqa.training.entities.DriverProvider;
import ru.stqa.training.pages.AdminLoginPage;
import ru.stqa.training.pages.AdminMenuPage;

import java.util.ArrayList;
import java.util.List;

/** Task 9.1
 * Verifies sorting of countries and states at the admin's Countries tab
 */
public class AdminMenuCountriesSortingTest {

    private String loginName = "admin";
    private String loginPassword = "admin";
    private DriverProvider.Browser driverType = DriverProvider.Browser.CHROME;

    private WebDriver driver;
    private WebDriverWait wait;
    static Logger log = LogManager.getLogger(AdminMenuCountriesSortingTest.class);

    @Before
    public void start() {
        driver = DriverProvider.createDriver(driverType);
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void test() {
        try {
            (new AdminLoginPage(driver)).doNavigateAndLogin(wait, loginName, loginPassword);
        } catch (Exception e) {
            log.error("[FAIL] Failed to detect login during the timeout");
            throw e;
        }
        log.info("Admin logged in successfully!");

        AdminMenuPage menuPage = new AdminMenuPage(driver);
        menuPage.switchToCountries();
        log.info("Switched to countries tab");

        List<WebElement> countries = menuPage.getCountriesList();
        if (countries.isEmpty()) {
            throw new ElementNotVisibleException("There is no countries in the list!");
        }

        List<String> countriesWithTimezonesURLs = new ArrayList<String>();
        String lastCountry = "";

        // --- Go through list and verify country name ordering
        log.info("Verifying {} countries sorting (A-Z)", countries.size());
        for (WebElement country : countries) {
            String countryName = menuPage.getCountryItemName(country);

            if (countryName.compareTo(lastCountry) < 0) {
                log.error("[FAILED] Compare  -- A-Z sorting failed %n");
                throw new WebDriverException(String.format("Wrong countries items sorting! %s vs %s", countryName, lastCountry));
            }

            int timezonesCount = menuPage.countCountryItemTimezones(country);
            if (timezonesCount > 0) {
                countriesWithTimezonesURLs.add(menuPage.getEditCountryURL(country));
                log.info("Country {} has {} timezones", countryName, timezonesCount);
            }

            lastCountry = countryName;
        }
        log.info("[OK] Countries sorting verified successfully!");

        // --- Go through countries edit pages of countries with timezones and check timezone ordering
        if (countriesWithTimezonesURLs.size() > 0) {
            log.info("Verifying timezones ordering!");
            for (String countryURL : countriesWithTimezonesURLs) {
                driver.navigate().to(countryURL);
                log.info("Navigated to {}", countryURL);

                List<WebElement> timezones = menuPage.getTimezonesNames();
                if (timezones.isEmpty()) {
                    throw new ElementNotVisibleException("There is no timezones at the page!");
                }

                String lastTimezone = "";
                for (WebElement timezone : timezones) {
                    String timezoneName = timezone.getText();

                    if (timezoneName.compareTo(lastTimezone) < 0) {
                        log.error(
                            "[FAILED] Timezone Compare for {} -- A-Z sortnig failed for {}"
                            , driver.findElement(By.xpath("//input[@name='name']")).getAttribute("name")
                            , timezoneName
                        );
                        throw new WebDriverException(String.format("Wrong timezones items sorting! %s vs %s", timezoneName, lastTimezone));
                    }

                    lastTimezone = timezoneName;
                }
            }
            log.info("[OK] All timezones verified successfully!");
        } else {
            log.info("No extra timezones to check sorting!");
        }

        log.info("[Success] All countries and timezones sorting verified successfully!");
    }

    @After
    public void stop() {
        driver.quit();
        driver = null;
    }
}


package ru.stqa.training.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.stqa.training.entities.Customer;
import ru.stqa.training.entities.DriverProvider;
import ru.stqa.training.pages.ShopMainPage;
import ru.stqa.training.pages.UserRegistrationPage;

import static org.junit.Assert.assertTrue;

/** Task 11
 * Verifies ability to register and log in as new shop user (customer)
 */
public class UserRegistrationTest {

    private String country = "US";
    private String state = "OH";

    private DriverProvider.Browser driverType = DriverProvider.Browser.CHROME;

    private WebDriver driver;
    private WebDriverWait wait;
    static Logger log = LogManager.getLogger(UserRegistrationTest.class);

    @Before
    public void start() {
        driver = DriverProvider.createDriver(driverType);
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void test() {
        ShopMainPage mainpage = new ShopMainPage(driver);

        try {
            mainpage.navigateTo();
            log.info("Navigated to Shop Main Page");

            mainpage.navigateToUserRegistration();
            log.info("Navigated to User registration page");
        } catch (Exception e) {
            log.error("[FAIL] Failed to navigate to user registration page");
            throw e;
        }

        // Creating customer using user-defined Country and State
        Customer customer = new Customer(country, state);

        UserRegistrationPage registrationPage = new UserRegistrationPage(driver);
        registrationPage.fullfilAndSubmitForm(customer);
        log.info("Registration submitted!");

        assertTrue("[FAIL] Failed to detected user logged in after registration!", mainpage.checkUserLogged());
        log.info("[OK] Registration done successfully");

        mainpage.logout();
        log.info("Logging out");

        mainpage.login(customer);
        log.info("Logging in");

        assertTrue("[FAIL] Failed to detected user logged in after registration!", mainpage.checkUserLogged());
        log.info("[OK] Logged in successfully!");

        log.info("[Success!] New user successfully registered and logged in!");
    }

    @After
    public void stop() {
        driver.quit();
        driver = null;
    }
}

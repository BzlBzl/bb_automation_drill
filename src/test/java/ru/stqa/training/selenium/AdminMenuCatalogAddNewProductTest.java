package ru.stqa.training.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.stqa.training.entities.CatalogProduct;
import ru.stqa.training.entities.DriverProvider;
import ru.stqa.training.pages.AdminLoginPage;
import ru.stqa.training.pages.AdminMenuAddNewProductPage;
import ru.stqa.training.pages.AdminMenuPage;

import java.util.List;

import static org.junit.Assert.assertTrue;

/** Task 12
 * Verifies ability to add new default product from admin menu (Catalog page)
 *
 * Note: Default product is used, but related imagePath should be specified.
 */

public class AdminMenuCatalogAddNewProductTest {

    private String loginName = "admin";
    private String loginPassword = "admin";
    private String imagePath = "src\\test\\resources\\productImage1.png";
    private DriverProvider.Browser driverType = DriverProvider.Browser.CHROME;

    private WebDriver driver;
    private WebDriverWait wait;
    static Logger log = LogManager.getLogger(AdminLoginTest.class);

    @Before
    public void start() {
        driver = DriverProvider.createDriver(driverType);
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void test() throws InterruptedException {

        try {
            (new AdminLoginPage(driver)).doNavigateAndLogin(wait, loginName, loginPassword);
        } catch (Exception e) {
            log.error("[FAIL] Failed to log in as an admin");
            throw e;
        }
        log.info("[OK] Admin logged in succesfully");

        AdminMenuPage menuPage = new AdminMenuPage(driver);
        menuPage.switchToCatalog();
        log.info("Switched to Catalog page");

        menuPage.clickAddNewProduct();
        log.info("Adding new product:");
        CatalogProduct product = new CatalogProduct(imagePath);
        log.info(product.toString());

        AdminMenuAddNewProductPage addProductPage = new AdminMenuAddNewProductPage(driver);
        addProductPage.fullfilAndSubmitForm(product, wait);
        log.info("[OK] Add new product form fullfiled and submitted");

        List<WebElement> catalogItems = menuPage.getCatalogProductList();
        boolean isProductAdded = false;
        for (WebElement item : catalogItems) {
            if ( menuPage.getCatalogProductItemName(item).equals(product.getName()) ) {
                isProductAdded = true;
            }
        }
        assertTrue("[FAIL] Failed to find added product in the catalog!", isProductAdded);
        log.info("[Success!] Product added successfully!");
    }

    @After
    public void stop() {
        driver.quit();
        driver = null;
    }
}

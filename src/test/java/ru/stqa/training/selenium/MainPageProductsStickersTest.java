package ru.stqa.training.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.stqa.training.entities.DriverProvider;
import ru.stqa.training.pages.ShopMainPage;

import java.util.List;

/** Task 8
 * Verifies presence of stickers for each product displayed on the main shop page
 */
public class MainPageProductsStickersTest {

    private DriverProvider.Browser browser = DriverProvider.Browser.CHROME;

    private WebDriver driver;
    private WebDriverWait wait;
    static Logger log = LogManager.getLogger(MainPageProductsStickersTest.class);

    @Before
    public void start() {
        driver = DriverProvider.createDriver(browser);
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void test() {
        ShopMainPage mainPage = new ShopMainPage(driver);
        try {
            mainPage.navigateTo();
        } catch (Exception e) {
            log.error("[FAIL] Failed to navigate to shop's main page");
            throw e;
        }

        List<WebElement> products = mainPage.getProductItems();

        if (products.isEmpty()) {
            log.error("[FAIL] No products at the main page!");
            throw new ElementNotVisibleException("No product items at the main page!");
        }

        for (WebElement product : products) {
            List<WebElement> stickers = mainPage.getProductItemStickers(product);
            String productName = product.findElement(By.className("name")).getText();

            log.info("[Checking product {}] Number of stickers: {}", productName, stickers.size());
            if (stickers.size() != 1) {
                log.error("[FAIL][Number of stickers is not 1 for product {}]", productName);
                throw new ElementNotVisibleException("Element has no single sticker");
            }

            log.debug("Single sticker is {}", stickers.get(0).getText());
        }

        log.info("[Success!] All product item's stickers were checked! Have a nice day!");
    }

    @After
    public void stop() {
        driver.quit();
        driver = null;
    }
}

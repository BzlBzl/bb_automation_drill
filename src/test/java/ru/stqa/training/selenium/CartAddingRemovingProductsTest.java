package ru.stqa.training.selenium;

import com.google.common.base.Function;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.*;
import ru.stqa.training.entities.DriverProvider;
import ru.stqa.training.pages.ShopCartPage;
import ru.stqa.training.pages.ShopMainPage;
import ru.stqa.training.pages.ShopProductPage;

import static java.lang.Integer.min;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.openqa.selenium.support.ui.ExpectedConditions.not;
import static org.openqa.selenium.support.ui.ExpectedConditions.textToBe;

/** Task 13
 *  Verifies ability to add several products to the cart and then remove them.
 */
public class CartAddingRemovingProductsTest {

    private By cartQuantityLabel = By.className("quantity");
    private DriverProvider.Browser driverType = DriverProvider.Browser.CHROME;

    private WebDriver driver;
    private WebDriverWait wait;
    static Logger log = LogManager.getLogger(AdminLoginTest.class);


    @Before
    public void start() {
        driver = DriverProvider.createDriver(driverType);
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void test() {
        ShopMainPage mainpage = new ShopMainPage(driver);
        try {
            mainpage.navigateTo();
            log.info("Navigated to Shop Main page");
        } catch (Exception e) {
            log.error("[FAIL] Failed to navigate to shop's main page");
        }

        ShopProductPage productPage = new ShopProductPage(driver);
        int mainPageProductsCount = mainpage.getProductItems().size();
        assertTrue("[FAIL] There is no products at main page", mainPageProductsCount > 0);

        int cartItemsCount;
        for (int i = 0; i < 3; i++) {
            cartItemsCount = mainpage.getCartItemsCount();
            mainpage.getProductItems().get(min(i, mainPageProductsCount - 1)).click();
            log.info("Navigated to product {} page", i);

            productPage.addProductToCart();
            wait.until(not(textToBe(cartQuantityLabel, Integer.toString(cartItemsCount))));
            log.info("Product added to cart");

            driver.navigate().back();
        }

        try {
            mainpage.navigateToCart();
            mainpage = null;
            productPage = null;
            log.info("Navigated to Checkout (Cart) page");
        } catch (Exception e) {
            log.error("[FAIL] Failed to navigate to Checkout (Cart) page");
            throw e;
        }

        ShopCartPage cartPage = new ShopCartPage(driver);
        int checkoutShortcutsCount = cartPage.getShortcutItems().size();
        int checkoutTableItemsCount = cartPage.getTableTotalQuantity();

        assertFalse("[FAIL] No item shortcuts found on the page", checkoutShortcutsCount == 0 && checkoutTableItemsCount == 0 );
        for (int i = 0; i < checkoutShortcutsCount; i++) {
            checkoutTableItemsCount = cartPage.getTableTotalQuantity();
            log.info("Checkout table has total of {} items", checkoutTableItemsCount, i);
            cartPage.clickRemoveItem();
            wait.until(getIsTableRefreshedFunction(checkoutTableItemsCount));
        }

        assertTrue("[FAIL] No empty cart message displayed", cartPage.isEmptyCartLabelDisplayed());
        log.info("[Success!] Items added and removed from cart successfully!");
    }

    @After
    public void stop() {
        driver.quit();
        driver = null;
    }

    public Function<WebDriver, Boolean> getIsTableRefreshedFunction(int compareVal) {
        return new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                return (new ShopCartPage(driver)).getTableTotalQuantity() != compareVal;
            }
        };
    }
}

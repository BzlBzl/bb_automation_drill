package ru.stqa.training.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.stqa.training.entities.DriverProvider;
import ru.stqa.training.pages.AdminLoginPage;

/** Task 7
 * Verifies that all tabs accessible in admin menu
 */
public class AdminMenuTest {
    private String loginName = "admin";
    private String loginPassword = "admin";
    private DriverProvider.Browser driverType = DriverProvider.Browser.CHROME;

    private WebDriver driver;
    private WebDriverWait wait;
    static Logger log = LogManager.getLogger(AdminMenuTest.class);

    @Before
    public void start() {
        driver = DriverProvider.createDriver(driverType);
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void test() {
        // --- Login to admin panel
        try {
            (new AdminLoginPage(driver)).doNavigateAndLogin(wait, loginName, loginPassword);
        } catch (Exception e) {
            log.error("[FAIL] Failed to log in as an admin");
            throw e;
        }

        log.info("[OK][Admin logged successfully!]");

        // --- Get all <li> items from "box-apps-menu" list and click through
        By tagH1 = By.tagName("h1");
        int menuItemsCount = driver.findElements(By.xpath("//ul[@id='box-apps-menu']/li")).size();

        if (menuItemsCount == 0) {
            log.error("[FAIL] No menu items found!");
            throw new ElementNotVisibleException("No menu elements at the page!");
        }

        String menuItemTitle;
        for (int i = 1; i <= menuItemsCount; i++) {
            driver.findElement(By.xpath(String.format("//ul[@id='box-apps-menu']/li[%d]", i))).click();

            try {
                wait.until(ExpectedConditions.presenceOfElementLocated(tagH1));
            } catch (Exception e) {
                log.error("[FAIL] Failed to find H1 element at the page");
                throw e;
            }

            menuItemTitle = driver.findElement(tagH1).getText();
            log.info("[{} page] opened successfully", menuItemTitle);

            // --- Get nested items and click through
            int subitemsCount = driver.findElements(By.xpath("//ul[@class='docs']/li")).size();

            for (int j = 1; j <= subitemsCount; j++) {
                driver.findElement(By.xpath(String.format("//ul[@class='docs']/li[%d]", j))).click();

                try {
                    wait.until(ExpectedConditions.presenceOfElementLocated(tagH1));
                } catch (Exception e) {
                    log.error("[FAIL] Failed to find H1 element at the sub-page");
                    throw e;
                }

                log.info(
                    "[{} page > {} sub-page] opened successfully"
                    , menuItemTitle
                    , driver.findElement(tagH1).getText()
                );
            }
        }

        log.info("[Success] All menu items clicked! Have a nice day!");
    }

    @After
    public void stop() {
        driver.quit();
        driver = null;
    }
}

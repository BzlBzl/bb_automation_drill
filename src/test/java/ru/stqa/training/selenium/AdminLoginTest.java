package ru.stqa.training.selenium;

import org.apache.logging.log4j.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.stqa.training.entities.DriverProvider;
import ru.stqa.training.pages.AdminLoginPage;


/** Task 3
 * Verifies ability to log in as admin
 */
public class AdminLoginTest {
    private String loginName = "admin";
    private String loginPassword = "admin";
    private DriverProvider.Browser driverType = DriverProvider.Browser.CHROME;

    private WebDriver driver;
    private WebDriverWait wait;
    static Logger log = LogManager.getLogger(AdminLoginTest.class);

    @Before
    public void start() {
        driver = DriverProvider.createDriver(driverType);
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void test() {
        AdminLoginPage loginPage = new AdminLoginPage(driver);

        try {
            loginPage.navigateTo();
            loginPage.login(loginName, loginPassword);
            loginPage.waitForLogin(wait);
        } catch (Exception e) {
            log.error("[FAIL][Failed to detect login during the timeout]");
            throw e;
        }

        log.info("[Success] Admin logged in successfully! Have a nice day!");
    }

    @After
    public void stop() {
        driver.quit();
        driver = null;
    }
}

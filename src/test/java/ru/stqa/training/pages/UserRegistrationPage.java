package ru.stqa.training.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import ru.stqa.training.entities.Customer;

public class UserRegistrationPage {

    private WebDriver driver;

    private By firstNameField = By.name("firstname");
    private By lastNameField = By.name("lastname");
    private By address1Field = By.name("address1");
    private By postcodeField = By.name("postcode");
    private By cityField = By.name("city");
    private By emailField = By.name("email");
    private By phoneField = By.name("phone");
    private By passwordField = By.name("password");
    private By passwordConfirmField = By.name("confirmed_password");

    private By submitButton = By.name("create_account");

    private By countryDropdown = By.xpath("//select[@name='country_code']");
    private By stateDropdown = By.xpath("//select[@name='zone_code']");

    public UserRegistrationPage(WebDriver driver) {
        this.driver = driver;
    }

    public void fullfilAndSubmitForm(Customer customer) {
        driver.findElement(firstNameField).sendKeys(customer.getFirstName());
        driver.findElement(lastNameField).sendKeys(customer.getLastName());
        driver.findElement(emailField).sendKeys(customer.getEmail());
        driver.findElement(passwordField).sendKeys(customer.getPassword());
        driver.findElement(passwordConfirmField).sendKeys(customer.getPassword());

        Select country = new Select(driver.findElement(countryDropdown));
        country.selectByValue(customer.getCountry());

        driver.findElement(address1Field).sendKeys(customer.getAddress());
        driver.findElement(postcodeField).sendKeys(customer.getPostcode());
        driver.findElement(cityField).sendKeys(customer.getCity());
        driver.findElement(phoneField).sendKeys(customer.getPhone());

        if (!customer.getState().isEmpty()) {
            Select state = new Select(driver.findElement(stateDropdown));
            state.selectByValue(customer.getState());
        }

        driver.findElement(submitButton).click();
    }


}

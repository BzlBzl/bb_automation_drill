package ru.stqa.training.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ru.stqa.training.entities.Customer;
import ru.stqa.training.entities.Product;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ShopMainPage {
    private WebDriver driver;

    private String url = "http://localhost/litecart/";
    private By productItem = By.className("product");
    private By stickerLabel = By.className("sticker");

    private By campaignProductsItem = By.xpath("//div[@id='box-campaigns']//a");

    private By registrationLink = By.xpath("//form[@name='login_form']//a");
    private By loginEmailField = By.xpath("//form[@name='login_form']//input[@name='email']");
    private By loginPasswordField = By.xpath("//form[@name='login_form']//input[@name='password']");
    private By loginButton = By.xpath("//form[@name='login_form']//button[@name='login']");
    private By logoutLink = By.xpath("//div[@id='box-account']//a[contains(@href,'logout')]");

    private By cartQuantityLabel = By.className("quantity");
    private By cartCheckoutLink = By.xpath("//div[@id='cart']/a[@class='link']");

    public ShopMainPage(WebDriver driver) {
        this.driver = driver;
    }

    public void navigateTo() {
        driver.navigate().to(url);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
    }

    public List<WebElement> getProductItems() {
        return driver.findElements(productItem);
    }

    public List<WebElement> getProductItemStickers(WebElement product) {
        return product.findElements(stickerLabel);
    }

    public List<WebElement> getCampaignProductsItems() {
        return driver.findElements(campaignProductsItem);
    }

    public Product getProduct(WebElement productItem) {
        WebElement priceNormal = productItem.findElement(By.className("regular-price"));
        WebElement pricePromo = productItem.findElement(By.className("campaign-price"));

        return new Product(
            productItem.findElement(By.className("name")).getText()
            , priceNormal.getText().substring(1)
            , pricePromo.getText().substring(1)
            , priceNormal.getCssValue("color")
            , pricePromo.getCssValue("color")
            , priceNormal.getCssValue("text-decoration-line")
            , pricePromo.getCssValue("text-decoration-line")
        );
    }

    public int getCartItemsCount() {
        return Integer.parseInt(driver.findElement(cartQuantityLabel).getText());
    }

    public void navigateToCart() {
        driver.findElement(cartCheckoutLink).click();
    }

    public void navigateToUserRegistration() {
        driver.findElement(registrationLink).click();
    }

    public void login(Customer customer) {
        driver.findElement(loginEmailField).sendKeys(customer.getEmail());
        driver.findElement(loginPasswordField).sendKeys(customer.getPassword());
        driver.findElement(loginButton).click();
    }

    public void logout() {
        driver.findElement(logoutLink).click();
    }

    public boolean checkUserLogged() {
        return driver.findElement(logoutLink).isDisplayed();
    }
}

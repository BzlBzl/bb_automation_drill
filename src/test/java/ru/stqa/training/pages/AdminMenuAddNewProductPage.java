package ru.stqa.training.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.stqa.training.entities.CatalogProduct;

public class AdminMenuAddNewProductPage {
    private WebDriver driver;

    private By saveButton = By.name("save");

    private By generalTab = By.xpath("//a[@href='#tab-general']");
    private By generalStatusRB = By.name("status");
    private By generalNameField = By.name("name[en]");
    private By generalCodeField = By.name("code");
    private By generalQuantityField = By.name("quantity");
    private By generalImage = By.name("new_images[]");

    private By infoTab = By.xpath("//a[@href='#tab-information']");
    private By infoKeywordsField = By.name("keywords");
    private By infoShortDescField = By.name("short_description[en]");
    private By infoLongDescField = By.name("description[en]");

    private By pricesTab = By.xpath("//a[@href='#tab-prices']");
    private By priceUSDField = By.name("prices[USD]");
    private By priceEURField = By.name("prices[EUR]");


    public AdminMenuAddNewProductPage(WebDriver driver) {
        this.driver = driver;
    }

    public void fullfilAndSubmitForm(CatalogProduct product, WebDriverWait wait) {
        driver.findElement(generalTab).click();

        driver.findElement(generalNameField).sendKeys(product.getName());
        driver.findElement(generalCodeField).sendKeys(product.getCode());
        driver.findElement(generalQuantityField).clear();
        driver.findElement(generalQuantityField).sendKeys(product.getQuantityText());
        driver.findElement(generalImage).sendKeys(product.getImagePath());

        if (product.isStatusEnabled()) {
            driver.findElements(generalStatusRB).get(0).click();
        } else {
            driver.findElements(generalStatusRB).get(1).click();
        }

        driver.findElement(infoTab).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(infoKeywordsField));

        driver.findElement(infoKeywordsField).sendKeys(product.getKeywords());
        driver.findElement(infoShortDescField).sendKeys(product.getShortDescription());
        driver.findElement(infoLongDescField).sendKeys(product.getLongDescription());

        driver.findElement(pricesTab).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(priceUSDField));

        driver.findElement(priceUSDField).sendKeys(product.getPriceUSDText());
        driver.findElement(priceEURField).sendKeys(product.getPriceEURText());

        driver.findElement(saveButton).click();
    }


}

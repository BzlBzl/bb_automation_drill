package ru.stqa.training.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class AdminLoginPage {

    private WebDriver driver;

    private String url = "http://localhost/litecart/admin";
    private By textboxLogin = By.name("username");
    private By textboxPassword = By.name("password");
    private By buttonLogin = By.tagName("button");

    public AdminLoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public void navigateTo() {
        driver.navigate().to(url);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
    }

    public void login(String login, String password) {
        driver.findElement(textboxLogin).sendKeys(login);
        driver.findElement(textboxPassword).sendKeys(password);
        driver.findElement(buttonLogin).click();
    }

    public void waitForLogin(WebDriverWait wait) {
        wait.until(presenceOfElementLocated(By.xpath("//a[@title='Logout']")));
    }

    public void doNavigateAndLogin(WebDriverWait wait, String login, String password) {
        navigateTo();
        login(login, password);
        waitForLogin(wait);
    }
}

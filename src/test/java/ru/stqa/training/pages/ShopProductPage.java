package ru.stqa.training.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import ru.stqa.training.entities.Product;

public class ShopProductPage {
    private WebDriver driver;

    private By productTitle = By.tagName("h1");
    private By priceNormal = By.className("regular-price");
    private By pricePromo = By.className("campaign-price");

    private By productSizeSelect = By.name("options[Size]");
    private By addProductButton = By.name("add_cart_product");

    public ShopProductPage(WebDriver driver) {
        this.driver = driver;
    }

    public Product getProduct() {
        WebElement priceNormalItem = driver.findElement(priceNormal);
        WebElement pricePromoItem = driver.findElement(pricePromo);

        return new Product(
            driver.findElement(productTitle).getText()
            , priceNormalItem.getText().substring(1)
            , pricePromoItem.getText().substring(1)
            , priceNormalItem.getCssValue("color")
            , pricePromoItem.getCssValue("color")
            , priceNormalItem.getCssValue("text-decoration-line")
            , pricePromoItem.getCssValue("text-decoration-line")
        );
    }

    public void addProductToCart() {
        if (!driver.findElements(productSizeSelect).isEmpty()) {
            // --- Selects first available product size if there are options present
            Select sizeSelect = new Select(driver.findElement(productSizeSelect));
            sizeSelect.selectByIndex(1);
        }

        driver.findElement(addProductButton).click();
    }
}

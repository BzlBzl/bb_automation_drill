package ru.stqa.training.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ShopCartPage {

    private WebDriver driver;

    private By shortcutItem = By.xpath("//ul[@class='shortcuts']/li/a");
    private By removeItemButton = By.name("remove_cart_item");
    private By tableQuantityItem = By.xpath("//div[@id='order_confirmation-wrapper']//tr[not(@class)]/td[1]");

    private By emptyCartLabel = By.tagName("em");

    public ShopCartPage(WebDriver driver) {
        this.driver = driver;
    }

    public List<WebElement> getShortcutItems() {
        return driver.findElements(shortcutItem);
    }

    public void clickRemoveItem() {
        driver.findElement(removeItemButton).click();
    }

    public int getTableTotalQuantity() {
        List<WebElement> tableQuantityCells = driver.findElements(tableQuantityItem);
        if (tableQuantityCells.isEmpty()) { return 0; }

        int quantity = 0;
        for (WebElement cell : tableQuantityCells) {
            String cellText = cell.getText();
            try {
                quantity = quantity + Integer.parseInt(cellText);
            } catch (Exception e) {
                // Suppressing
            }
        }

        return quantity;
    }

    public boolean isEmptyCartLabelDisplayed() {
        return !(driver.findElements(emptyCartLabel).isEmpty());
    }
}

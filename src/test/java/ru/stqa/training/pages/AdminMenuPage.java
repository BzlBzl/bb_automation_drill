package ru.stqa.training.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class AdminMenuPage {
    WebDriver driver;

    private String url = "http://localhost/litecart/admin/";

    // --- Countries
    private By countriesMenuButton = By.xpath("//ul[@id='box-apps-menu']/li[@id='app-']/a[contains(@href,'?app=countries')]");
    private By countriesListItems = By.xpath("//form[@name='countries_form']//tr[@class='row']");
    private By countriesListItemName = By.xpath(".//td[5]");
    private By countriesListItemEdit = By.xpath(".//a");
    private By countriesListItemTimezones = By.xpath(".//td[6]");
    private By timezoneListItemName = By.xpath("//table[@id='table-zones']//tr[not(@class)]/td[3]/input[not(@value='')]/..");
    private By addNewCountryButton = By.xpath("//td/div/a[contains(@href, 'doc=edit_country')]");

    // --- Geozones
    private By geozonesMenuButton = By.xpath("//ul[@id='box-apps-menu']/li[@id='app-']/a[contains(@href,'?app=geo_zones')]");
    private By geozonesItemURL = By.xpath("//form[@name='geo_zones_form']//td[3]/a");
    private By geozonesSunitemsName = By.xpath("//form[@name='form_geo_zone']//select[contains(@name,'zone_code')]/option[@selected]");

    // --- Catalog
    private By catalogMenyButton = By.xpath("//ul[@id='box-apps-menu']/li[@id='app-']/a[contains(@href,'?app=catalog')]");
    private By catalogAddNewProductButton = By.xpath("//td[@id='content']/div/a[contains(@href,'doc=edit_product')]");
    private By catalogProductListItems = By.xpath("//form[@name='catalog_form']/table/tbody/tr[contains(@class,'row')]");
    private By catalogProductListItemName = By.xpath(".//td[3]");


    public AdminMenuPage(WebDriver driver) {
        this.driver = driver;
    }

    public void navigateTo() {
        driver.navigate().to(url);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
    }

    // --- Countries
    public void switchToCountries() {
        driver.findElement(countriesMenuButton).click();
    }

    public List<WebElement> getCountriesList() {
        List<WebElement> countriesNames = driver.findElements(countriesListItems);
        return countriesNames;
    }

    public String getCountryItemName(WebElement countryItem) {
        return countryItem.findElement(countriesListItemName).getText();
    }

    public int countCountryItemTimezones(WebElement countryItem) {
        return Integer.parseInt(countryItem.findElement(countriesListItemTimezones).getText());
    }

    public String getEditCountryURL(WebElement countryItem) {
        return countryItem.findElement(countriesListItemEdit).getAttribute("href");
    }

    public List<WebElement> getTimezonesNames() {
        return driver.findElements(timezoneListItemName);
    }

    public void clickAddNewCountry() {
        driver.findElement(addNewCountryButton).click();
    }

    // --- Geozones
    public void switchToGeozones() {
        driver.findElement(geozonesMenuButton).click();
    }

    public List<String> getGeozonesURLsList() {
        List<String> urls = new ArrayList<>();

        List<WebElement> arefs = driver.findElements(geozonesItemURL);
        for (WebElement aref : arefs) {
            urls.add(aref.getAttribute("href"));
        }

        return urls;
    }

    public List<String> getGeozoneSubitemSelectedNames() {
        List<String> names = new ArrayList<>();
        List<WebElement> subitems = driver.findElements(geozonesSunitemsName);

        for (WebElement item : subitems) {
            names.add(item.getText());
        }

        return names;
    }

    // --- Catalog
    public void switchToCatalog() {
        driver.findElement(catalogMenyButton).click();
    }

    public void clickAddNewProduct() {
        driver.findElement(catalogAddNewProductButton).click();
    }

    public List<WebElement> getCatalogProductList() {
        return driver.findElements(catalogProductListItems);
    }

    public String getCatalogProductItemName(WebElement catalogProductItem) {
        return  catalogProductItem.findElement(catalogProductListItemName).getText();
    }
}

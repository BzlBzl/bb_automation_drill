package ru.stqa.training.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class AdminMenuAddNewCountryPage {
    private WebDriver driver;

    private By outerLinks = By.xpath("//form//a[@target='_blank']");

    public AdminMenuAddNewCountryPage(WebDriver driver) {
        this.driver = driver;
    }

    public List<WebElement> getOuterLinks() {
        return driver.findElements(outerLinks);
    }

}

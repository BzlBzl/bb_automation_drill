package ru.stqa.training.entities;

public class Customer {

    private static int id = 0;

    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String address;
    private String city;
    private String postcode;
    private String phone;
    private String country;
    private String state;

    private int customerID;

    public Customer() {
        firstName = "John";
        lastName = "Doe";
        address = "Default Address";
        city = "Default-City";
        postcode = "25222";
        phone = "+1222334445566";
        country = "US";
        state = "OH";
        password = "password";

        email = generateEmail();
        customerID = id;
        id++;
    }

    public Customer(String country, String state) {
        this();
        this.country = country;
        this.state = state;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getPhone() {
        return phone;
    }

    public String getCountry() {
        return country;
    }

    public String getState() {
        return state;
    }

    private String generateEmail() {
        return String.format("testmail_%d_%d@testmail.xxx", customerID, System.currentTimeMillis());
    }

    @Override
    public String toString() {
        return String.format("Customer@%d: %s %s, %s/%s; %s, %s, %s, t.%s, %s (%s)", customerID, firstName, lastName, email, password, city, address, postcode, phone, country, state);
    }
}

package ru.stqa.training.entities;

import java.io.File;

/** Represent Product in admin Catalog
 */
public class CatalogProduct {
    static private int id = 0;
    private int productId;

    private boolean statusEnabled;
    private String name;
    private String code;
    private int quantity;
    private String imagePath;

    private String keywords;
    private String shortDescription;
    private String longDescription;

    private float priceUSD;
    private float priceEUR;

    /** Creates CatalogProduct with user-specified values
     *
     * @param statusEnabled The product's availability state (should product be displayed at the main shop page)
     * @param name The product's display name, if "" - name will be generated in format "Test Product ID-Timestamp"
     * @param code The product's code name, if "" - code will be generated in format "TESTPRID-Timestamp"
     * @param quantity The amount of product items stored
     * @param imagePath Relative path to product's image file
     * @param keywords
     * @param shortDescription
     * @param longDescription
     * @param priceUSD
     * @param priceEUR
     */
    public CatalogProduct(boolean statusEnabled, String name, String code, int quantity, String imagePath
            , String keywords, String shortDescription, String longDescription, float priceUSD, float priceEUR) {

        this.statusEnabled = statusEnabled;
        this.quantity = quantity;
        this.imagePath = imagePath;
        this.keywords = keywords;
        this.shortDescription = shortDescription;
        this.longDescription = longDescription;
        this.priceUSD = priceUSD;
        this.priceEUR = priceEUR;
        this.productId = id;

        this.name = name.isEmpty() ? generateName() : name;
        this.code = code.isEmpty() ? generateCode() : code;

        id++;
    }

    /** Creates CatalogProduct with defaults values
     * @param imagePath Relative path to image file
     */
    public CatalogProduct(String imagePath) {
        this(true, "","", 12, imagePath
                , "key1,key2", "Short description", "Full description"
                , 15, 14
        );
    }

    private String generateName() {
        return String.format("Test Product %d-%d", productId, System.currentTimeMillis());
    }

    private String generateCode() {
        return String.format("TESTPR%d-%d", productId, System.currentTimeMillis());
    }

    public boolean isStatusEnabled() {
        return statusEnabled;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public String getQuantityText() {
        return Integer.toString(quantity);
    }

    /** Gets absolute image path
     * @return absolute image path
     */
    public String getImagePath() {
        return new File(imagePath).getAbsolutePath();
    }

    public String getKeywords() {
        return keywords;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public String getPriceUSDText() {
        return Float.toString(priceUSD);
    }

    public String getPriceEURText() {
        return Float.toString(priceEUR);
    }

    @Override
    public String toString() {
        return String.format(
                "CatalogProduct@%d: (%b) %s [%s], %fUSD, %fEUR - %d pcs"
                , productId
                , statusEnabled
                , name
                , code
                , priceUSD
                , priceEUR
                , quantity
        );
    }
}

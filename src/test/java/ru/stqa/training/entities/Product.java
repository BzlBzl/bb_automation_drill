package ru.stqa.training.entities;

import org.openqa.selenium.support.Color;

public class Product {

    private String name;
    private float priceNormal;
    private float pricePromo;
    private String priceNormalTextColor;
    private String priceNormalTextDecoration;
    private String pricePromoTextColor;
    private String pricePromoTextDecoration;

    public Product(String name, String priceNormal, String pricePromo,
                   String priceNormalTextColor, String pricePromoTextColor,
                   String priceNormalTextDecoration, String pricePromoTextDecoration
    ) {
        this.name = name;
        this.priceNormal = Float.parseFloat(priceNormal);
        this.pricePromo = Float.parseFloat(pricePromo);
        this.priceNormalTextColor = Color.fromString(priceNormalTextColor).asHex();
        this.pricePromoTextColor = Color.fromString(pricePromoTextColor).asHex();
        this.priceNormalTextDecoration = priceNormalTextDecoration;
        this.pricePromoTextDecoration = pricePromoTextDecoration;
    }

    public String getName() {
        return name;
    }

    public float getPriceNormal() {
        return priceNormal;
    }

    public float getPricePromo() {
        return pricePromo;
    }

    public boolean verifyPromo2NormalPriceRatio() {
        return priceNormal > pricePromo;
    }

    public boolean verifyPromoPriceDecoration() {
        // --- Verifies that color is Red (e.g. all pairs are 00 except of first)
        if (
            pricePromoTextColor.substring(1,3).equals("00")
            || !(pricePromoTextColor.substring(3).equals("0000"))
        ) {
            return false;
        }

        return true;
    }

    public boolean verifyNormalPriceDecoration() {
        // --- Verifies that color is gray (e.g. all pairs are the same #060606, but no #FF0000)
        for (int i = 3; i < priceNormalTextColor.length(); i += 2) {
            if (
                priceNormalTextColor.charAt(i) != priceNormalTextColor.charAt(1)
                || priceNormalTextColor.charAt(i+1) != priceNormalTextColor.charAt(2)
            ) {
                return false;
            }
        }

        // --- Verifies text decoration
        if (!priceNormalTextDecoration.equals("line-through")) {
            return false;
        }

        return true;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Product)) {
            return false;
        }

        Product product = (Product) obj;
        return (
            name.equals(product.getName())
            && priceNormal == product.getPriceNormal()
            && pricePromo == product.getPricePromo()
        );
    }

    @Override
    public String toString() {
        return String.format("[%s, normal price: %f, promo price: %f]", name, priceNormal, pricePromo);
    }
}

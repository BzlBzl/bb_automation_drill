package ru.stqa.training.entities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;

/** Wrapper over WebDriver creation to create web driver of any type same way
 */
public class DriverProvider {
    public enum Browser {
        CHROME
        , FIREFOX
        , FIREFOX_NIGHTLY
        , FIREFOX_ESR
        , INTERNET_EXPLORER
    }

    public static WebDriver createDriver(Browser type) {
        WebDriver driver = null;
        switch (type) {
            case CHROME: {
                driver = new ChromeDriver();
                break;
            }
            case FIREFOX: {
                driver = new FirefoxDriver();
                break;
            }
            case FIREFOX_NIGHTLY: {
                System.setProperty(FirefoxDriver.SystemProperty.BROWSER_BINARY, "C:\\Program Files\\Firefox Nightly\\firefox.exe");
                driver = new FirefoxDriver();
                break;
            }
            case FIREFOX_ESR: {
                // Doesn't work for some reason after installing FF Nightly. I suppose ZOG is involved ;,(
                FirefoxOptions options = new FirefoxOptions().setLegacy(true);
                driver = new FirefoxDriver(options);
                break;
            }
            case INTERNET_EXPLORER: {
                driver = new InternetExplorerDriver();
                break;
            }
        }

        return driver;
    }
}
